---
title: 'Administrateur Réseau'
media_order: 'administration-reseau.jpg,logo-ou.png,logo-qui.png,logo-quoi.png,logo-pourquoi.png,logo-comment.jpg,logo-quand.png'
taxonomy:
    category:
        - docs
visible: true
textsize:
    scale: ''
    modifier: ''
style:
    header-font-family: ''
    header-color: ''
    block-font-family: ''
    block-color: ''
    background-color: ''
    background-image: ''
    background-size: ''
    background-repeat: ''
    justify-content: ''
    align-items: ''
class: ''
footer: ''
presentation:
    content: ''
    parser: ''
    styles: ''
---

![](administration-reseau.jpg)
## Objectif de la documentation
---
L'objectif de cette documentation est de faire découvrir l'univers d'un administrateur de réseau.

## Description des tâches
---

* _Quoi:_ ![img](logo-quoi.png)
	* Le métier d'administrateur réseau consiste à **être en charge du réseau informatique** d'une entité.
	
* _Qui:_ ![img](logo-qui.png)
	* Afin de devenir administrateur réseau, **au Québec**, il est recommendé d'avoir un **diplôme d'étude collégial ou un baccalauréat**.
	
   
* _Où:_ ![img](logo-ou.png)
	* Le métier d'administrateur réseau se fait très souvent à l'**intérieurs d'entreprises où il y a plusieurs postes de travails** à gérer.
	
* _Quand:_ ![img](logo-quand.png)
	* Même si les postes d'administrateurs réseaux **peuvent être accèssible aux jeunes diplômés**, ils s'adressent majoritairement à ceux ayant déjà de **l'expérience professionnelle** dans le domaine.

* _Comment:_ ![img](logo-comment.jpg)
	* Pour devenir un bon administrateur réseau, il faut quelqu'un de **débrouillard** et qui aime résoudre des **problèmes techniques**.
	* Il est préférable d'être **bilingue**.

* Pourquoi: ![img](logo-pourquoi.png)
	* Un administrateur réseau met des procédures en place afin d'**améliorer l'efficacité des réseaux** de l'entité.

 ### Exemple de travail
 
[plugin:youtube](https://www.youtube.com/watch?v=ERHbDIsIAN4)

---

#### [Emplois dans le domaine](../emplois)

---

#### Sources
[APEC](https://www.apec.fr/tous-nos-metiers/informatique/administrateur-reseau.html)

[Emplois Montréal](https://www.emploistimontreal.com/professions-ti/profession-ti-administrateur-reseau-16)

[Wikipedia](https://fr.wikipedia.org/wiki/Administrateur_r%C3%A9seau)

